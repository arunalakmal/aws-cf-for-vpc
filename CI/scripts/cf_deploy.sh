#!/usr/bin/env bash

echo "Checking if stack exists ..."
if ! aws cloudformation describe-stacks --region "$AWS_REGION" --stack-name "$STACK_NAME"; then
echo -e "\nStack does not exist, creating ..."
  aws cloudformation create-stack --region "$AWS_REGION" --stack-name "$STACK_NAME" --template-body file://amazon-eks-vpc-private-subnets.yaml

echo "Waiting for stack to be created ..."
  aws cloudformation wait stack-create-complete --region "$AWS_REGION" --stack-name "$STACK_NAME"
else
echo -e "\nStack exists, attempting update ..."
set +e
  update_output=$( aws cloudformation update-stack --region "$AWS_REGION" --stack-name "$STACK_NAME" --template-body file://amazon-eks-vpc-private-subnets.yaml 2>&1)
  status=$?
set -e
echo "$update_output"
if [ $status -ne 0 ] ; then
if [[ $update_output == *"ValidationError"* && $update_output == *"No updates"* ]] ; then
echo -e "\nFinished create/update - no updates to be performed"
exit 0
else
exit $status
fi
fi
echo "Waiting for stack to be updated ..."
  aws cloudformation wait stack-update-complete --region "$AWS_REGION" --stack-name "$STACK_NAME"
fi
echo "Finshed the create/update process successfully!"